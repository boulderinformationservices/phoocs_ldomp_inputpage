<?php
class InputPage
{
	const CHOOSER    = 0;
	const ALL_JSON   = 1;
	const STAY_HERE  = 2;
	const CLOSE_SELF = 3;
	public $dataObjectClass = ""; 	// e.g., public $dataObjectClass = "DO_movie";
	protected $requestedId = array(); // Copy this to your child class or you will be sorry
	
	protected function goChooser() { /* e.g., header("Location: choose_movie.php"); */ }
	protected function getPostedObject() 
	{
		/* e.g., 
		$new_DO = new $this->dataObjectClass();
		$new_DO->idMovie        = getRequest('movie_idMovie', 'POST', ''); 
		$new_DO->Title          = getRequest('movie_Title', 'POST', '');
		$new_DO->TitleNoArticle = getRequest('movie_TitleNoArticle', 'POST', '');

		// ... 

		$new_DO->ReleaseDate    = getRequest('movie_ReleaseDate', 'POST', '');
		return $new_DO;
		*/
	}

	function __construct()
	{
		$this->update = false;	// change to true if we are updating instead of inserting

		$this->afterInsert = self::CHOOSER;

		$this->new_DO = new $this->dataObjectClass();

		$this->pks = $this->new_DO->getPrimaryKeys();
		
		// if form has been submitted, insert the contact
		if (
			getRequest('Insert', 'POST', '') == 'Submit' ||
			getRequest('CreateCopy', 'POST', '') == 'Create a copy'
		   ) 
		{
			$this->updateAfterInsert();
			$success = $this->insertPostedObject();
			$this->doAfterInsert();
		}

		// if UPDATE form has been submitted, update the contact
		if (getRequest('Update', 'POST', '') == 'Submit') 
		{
			$this->updateAfterInsert();
			$success = $this->updatePostedObject();
			$this->doAfterInsert();
		}
		
		if (getRequest('UpdatePartial', 'POST', '') == 'Submit') 
		{
			$this->updateAfterInsert();
			$success = $this->partialUpdatePostedObject();
			$this->doAfterInsert();
		}
		
		if (getRequest('Delete', 'POST', '') == 'Delete') 
		{
			$this->updateAfterInsert();
			$success = $this->deletePostedObject();
			$this->doAfterInsert();
		}

		// if we've been passed a contact id, pre-fill the form
		foreach ($this->pks as $pk) 
		{
			if (getRequest($pk, 'GET', '') != '') 
			{
				$this->prefillForm();
				break;
			}
		}
	}
	
	public function prefillForm()
	{
		$this->our_DO = new $this->dataObjectClass();
		$search = new $this->dataObjectClass();
		$allKeysPresent = TRUE;
		foreach ($this->pks as $pk) 
		{
			$key = getRequest($pk, 'BOTH', '');
			$search->$pk = $key;
			$this->our_DO->$pk = $key;
			if (!$key) { $allKeysPresent = FALSE; }
		}
		$rs = $search->find();
		if ($allKeysPresent && (1 == $rs->rowCount())) 
		{
			$this->our_DO = $rs->getNext(new $this->dataObjectClass());
			$this->update = true;
		}
	}

	public function doAfterInsert()
	{
		if ($this->afterInsert == self::CHOOSER)
		{
			$this->goChooser();
		}
		if ($this->afterInsert == self::ALL_JSON)
		{
			$this->writeAllDataObjectsJson();
		}
		if ($this->afterInsert == self::STAY_HERE)
		{
			// $this->prefillForm();	// misleading - looks like we preload the page but we really don't
			$this->our_DO = $this->getPostedObject();
		}
		if ($this->afterInsert == self::CLOSE_SELF)
		{
			$this->our_DO = $this->closeSelf();
		}
	}
	
	public function writeAllDataObjectsJson()
	{
		$o = new $this->dataObjectClass();
		?>
		<script>
		if (window.opener && window.opener.maintainer && window.opener.maintainer.afterPopup)
		{
			window.opener.maintainer.afterPopup(<?php echo $o->findJson(); ?>);
			window.close();
		}
		</script>
		<?php
		exit;
	}

	protected function closeSelf()
	{
		$o = new $this->dataObjectClass();
		?>
		<script>
		if (window.opener)
		{
			window.close();
		}
		</script>
		<?php
		exit;
	}
	
	public function setAfterInsert($action)
	{
		if (
			$action == self::CHOOSER || 
			$action == self::ALL_JSON || 
			$action == self::STAY_HERE ||
			$action == self::CLOSE_SELF
		   )
		{
			$this->afterInsert = $action;
		}
		return $this->afterInsert;
	}
	
	public function updateAfterInsert()
	{
		if (getRequest('afterSubmit', 'POST', '') != '')
		{
			$this->setAfterInsert(getRequest('afterSubmit', 'POST', ''));
		}
	}
	
	protected function insertPostedObject()
	{
		$this->new_DO = $this->getPostedObject();
		return $this->new_DO->insert();
	}
	
	protected function updatePostedObject()
	{
		$this->new_DO = $this->getPostedObject();
		$this->new_DO->update();
	}
	
	protected function partialUpdatePostedObject()
	{
		$this->new_DO = $this->getPostedObject();
		$this->new_DO->updatePartial();
	}
	
	protected function deletePostedObject()
	{
		$this->new_DO = $this->getPostedObject();
		$this->new_DO->del();
	}
	
	/**
	 * Say whether to pre-select a foreign record, given its ID.
	 * 
	 * For example, given [1234, movie_idmovie], say whether 
	 * $this->our_DO->movie_idmovie == 1234, or whether 
	 * $_GET['movie_idmovie'] == 1234 -- presumably so that we can pre-select
	 * that movie from a dropdown.
	 * 
	 * @param $id
	 * @param $localField fieldname of $this->our_DO
	 * @see getRequestedForeignId()
	 */
	protected function toSelectForeign($id, $localField)
	{
		if 
		(
			($this->update == true) && 
			$this->our_DO->{$localField} && 
			$this->our_DO->{$localField} == $id
		)
		{
			return TRUE;
		}
		return ($id == $this->getRequestedForeignId($localField));
	}
	
	/**
	 * @see toSelectForeign()
	 */
	protected function getRequestedForeignId($localField)
	{
		if (!isset($this->requestedId[$localField]))
		{
			$this->requestedId[$localField] = getRequest($localField, 'get');
		}
		return $this->requestedId[$localField];
	}
}
